import React from 'react'
//const {string} = React.PropTypes // zamiast React.PropTypes.string
const Message2 = (props) => {
    return(
            <h1>{props.message}</h1>
        )
}
// Message2.PropTypes :{
//         message = string
//     }

const Message3 = (props) => <h1>{props.message}</h1>
       
const Message = React.createClass({
    propTypes:{
        message : React.PropTypes.string
    },
    render()
    {
        return(
            <h1>{this.props.message}</h1>
        )
    }
})
export default Message