import React from 'react'
import Message from './Message'

const Button = React.createClass({
    getInitialState(){
        return {
            onClick:React.PropTypes.function 
                }
    },
    render(){
        return(            
            <div>
                <button onClick={()=> this.props.onClick }> Count down </button>
            </div>
        )       
    }
})
const MyFirstComponent = React.createClass({
    getInitialState(){
        return {
            value:0
        }
    },
    onClick(){
        console.log('click')        
        this.setState({
            value: this.state.value + 1
        })//,function(){}) state doesnt have to change in this line due to optimalization
        
    },
    onClickMinus(){
        this.setState({
            value : this.state.value - 1
        })
    },
    render(){
        return(
            <div>
            <Message message={this.props.value}/>
                <Button onClick= {this.onclick}/>
            </div>
        )       
    }
})
export default MyFirstComponent
//this.props this.state.value || this.setState({value:1})