import React from 'react'

const Todo = ({text, completed, markCompleted}) => {
  const style = completed ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle'
  return (
    <a href='#' className='task-item'>
      <span className='task-check' onClick={markCompleted}>
        <span className={'glyphicon ' + style} />
      </span>
      <span className='task-inner'>{text}</span>

    </a>
  )
}

Todo.propTypes = {
  text: React.PropTypes.string,
  completed: React.PropTypes.bool,
  markCompleted: React.PropTypes.func
}
export default Todo
